# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include pm_angelar_monitoring
class pm_angelar_monitoring {
  package { 'prometheus-node-exporter':
    ensure => latest
  }
}
